(ns pivo.category
  (:require ["react-native" :as rn]
            ["react-native-ratings" :as ratings]
            [pivo.styles.colors :refer [pale-yellow text-dark]]
            [pivo.components.card :refer [Card]]
            [pivo.assets.mug :refer [mug]]))

(defn Category [{:keys [navigation]}]
  (fn [{:keys [navigation]}]
    [:> rn/SafeAreaView {:style {:flex 1}}
     [:> rn/ScrollView {:style {:background-color pale-yellow}
                        :contentContainerStyle {:align-items :center}}

      [:> rn/Image {:source {:uri "https://golfodinapoli.ro/wp-content/uploads/2023/02/unnamed-file-16.png"}
                    :style {:width 300 :height 600}}]

      [:> rn/Text {:style {:font-weight   :bold
                           :font-size     32
                           :color text-dark
                           :font-family :westiva-bold}} "Birra Moretti"]
      [:> ratings/Rating {:type "heart" :ratingImage mug
                          :tintColor "#F4EBE8"
                          :ratingCount 5 :startingValue 4
                          :ratingBackgroundColor "#F4EBE8"}]

      [:> rn/Text {:style {:font-size 28
                           :color text-dark
                           :font-family :westiva-regular}} "Average price: €2"]
      [:> rn/View {:style {:padding-top 30 :flex 2 :align-items :center}}]
      [:f> Card
       [:> rn/Text {:style {:font-size 18
                            :color text-dark
                            :font-weight :bold
                            :font-family :westiva-bold}} "@mihaimiuta"]

       [:> rn/Text {:style {:font-size 14
                            :color text-dark
                            :font-family :westiva-regular}} "Great quality - price ratio!"]]
      [:f> Card
       [:> rn/Text {:style {:font-size 18
                            :color text-dark
                            :font-weight :bold
                            :font-family :westiva-bold}} "@pogmothoin"]

       [:> rn/Text {:style {:font-size 14
                            :color text-dark
                            :font-family :westiva-regular}} "Not really sure about this one..."]]

      [:f> Card
       [:> rn/Text {:style {:font-size 18
                            :color text-dark
                            :font-weight :bold
                            :font-family :westiva-bold}} "@danieltheone"]

       [:> rn/Text {:style {:font-size 14
                            :color text-dark
                            :font-family :westiva-regular}} "Really loved it!"]]]]))
