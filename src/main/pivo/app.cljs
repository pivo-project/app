(ns pivo.app
  (:require [expo.root :as expo-root]
            ["@react-navigation/native" :as native]
            ["@react-navigation/native-stack" :as native-stack]
            [pivo.welcome :refer [Welcome]]
            [pivo.login :refer [Login]]
            [pivo.register :refer [Register]]
            [pivo.home :refer [Home]]
            [pivo.history :refer [History]]
            [pivo.category :refer [Category]]
            [reagent.core :as r]))

(defonce Stack (native-stack/createNativeStackNavigator))

(defn- make-component [component] (fn [props] (r/as-element [component {:navigation (.-navigation props)}])))

(defn root []
  [:> native/NavigationContainer
   [:> (.-Navigator Stack) {:initialRouteName "Welcome"
                            :screenOptions {:headerShown false}}
    [:> (.-Screen Stack) {:name "Welcome" :key "welcome-screen" :component (make-component Welcome)}]
    [:> (.-Screen Stack) {:name "Login" :key "login-screen" :component (make-component Login)}]
    [:> (.-Screen Stack) {:name "Register" :key "register-screen" :component (make-component Register)}]
    [:> (.-Screen Stack) {:name "Home" :key "home-screen" :component (make-component Home)}]
    [:> (.-Screen Stack) {:name "History" :key "history-screen" :component (make-component History)}]
    [:> (.-Screen Stack) {:name "Category" :key "category-screen" :component (make-component Category)}]]])

(defn start
  {:dev/after-load true}
  []
  (expo-root/render-root (r/as-element [root])))

(defn init []
  (start))

