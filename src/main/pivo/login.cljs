(ns pivo.login
  (:require ["react-native" :as rn]
            [reagent.core :as r]
            [pivo.common.centered-container :refer [CenteredContainer]]
            [pivo.login.title :refer [Title]]
            [pivo.login.username :refer [UserName]]
            [pivo.login.password :refer [Password]]
            [pivo.login.submit :refer [Submit]]
            [pivo.login.back :refer [Back]]))

(defn Login [{:keys [navigation]}]
  (let [username (r/atom "") password (r/atom "")]
    (fn [{:keys [navigation]}]
      [CenteredContainer
       [Title]
       [:> rn/View {:style {:width "100%"}}
        [UserName {:username username}]
        [Password {:password password}]]
       [Submit {:username username :password password :navigation navigation}]
       [Back {:navigation navigation}]])))
