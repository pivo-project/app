(ns pivo.history
  (:require ["react-native" :as rn]
            [pivo.styles.colors :refer [pale-yellow text-dark]]))

(defn History [{:keys [navigation]}]
  (fn [{:keys [navigation]}]
    [:> rn/View {:style {:flex 1
                         :justify-content :center
                         :align-items :center
                         :background-color pale-yellow}}
     [:> rn/Text {:style {:font-size 24
                          :font-family :westiva-bold
                          :font-color text-dark}} "History"]]))
