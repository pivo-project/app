(ns pivo.register.submit
  (:require
   [reagent.core :as r]
   [pivo.components.button :refer [Button]]
   [pivo.graph.client :refer [make-client]]
   [pivo.graph.mutations.register :as mutations]
   [pivo.styles.colors :refer [text-dark]]))

(defn- register [username password]
  (let [client (make-client)]
    ((.-mutate client) #js {:mutation mutations/register
                            :variables #js {:username @username
                                            :password @password}})))

(defn- set-loading [loading] (reset! loading true))

(defn- on-press [username password loading]
  ((set-loading loading)
   (register username password)))

(defn- disabled [username password confirm-password loading]
  (or (= @username "") (= @password "") (= @confirm-password "") (= @loading true)))

(defn Submit [{:keys [username password confirm-password]}]
  (let [loading (r/atom false)]
    [Button {:disabled? (disabled username password confirm-password loading)
             :on-press (fn [] (on-press username password loading))
             :style {:background-color text-dark
                     :border-color text-dark
                     :border-width :2
                     :margin-top 20}
             :text-style {:font-family :westiva-bold
                          :color "white"
                          :font-weight :bold
                          :font-size 20}}
     "Register"]))
