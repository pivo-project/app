(ns pivo.register.back
  (:require [pivo.components.button :refer [Button]]
            [pivo.styles.colors :refer [pale-yellow text-dark]]))

(defn Back [{:keys [navigation]}]
  [Button {:disabled? false
           :on-press (fn [] ((.-navigate navigation) "Welcome"))
           :style {:background-color pale-yellow
                   :border-color text-dark
                   :border-width :2
                   :margin-top 5}
           :text-style {:font-family :westiva-bold
                        :color text-dark
                        :font-weight :bold
                        :font-size 20}}
   "Go back"])
