(ns pivo.register
  (:require ["react-native" :as rn]
            [reagent.core :as r]
            [pivo.common.centered-container :refer [CenteredContainer]]
            [pivo.register.title :refer [Title]]
            [pivo.register.username :refer [UserName]]
            [pivo.register.password :refer [Password]]
            [pivo.register.confirm-password :refer [ConfirmPassword]]
            [pivo.register.submit :refer [Submit]]
            [pivo.register.back :refer [Back]]))

(defn Register [{:keys [navigation]}]
  (let [username (r/atom "") password (r/atom "") confirm-password (r/atom "")]
    (fn [{:keys [navigation]}]
      [CenteredContainer
       [Title]
       [:> rn/View {:style {:width "100%"}}
        [UserName {:username username}]
        [Password {:password password}]
        [ConfirmPassword {:confirm-password confirm-password}]]
       [Submit {:username username :password password}]
       [Back {:navigation navigation}]])))
