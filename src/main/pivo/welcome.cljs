(ns pivo.welcome
  (:require [pivo.startup.fontWrapper :refer [font-wrapper]]
            [pivo.components.button :refer [Button]]
            ["react-native" :as rn]
            [pivo.styles.colors :refer [text-dark pale-yellow]]
            [pivo.assets.logo :refer [logo]]))

;; TODO: This component should be moved into its own folder
(defn Welcome [{:keys [navigation]}]
  (fn [{:keys [navigation]}]
    [:f> font-wrapper
     [:> rn/View {:style {:flex 1
                          :padding-vertical 50
                          :justify-content :center
                          :align-items :center
                          :background-color pale-yellow}}
      [:> rn/View {:style {:align-items :center}}
       [:> rn/Image {:style {:width 160 :height 160} :source logo}]
       [:> rn/Text {:style {:font-weight   :bold
                            :font-size     32
                            :color text-dark
                            :font-family :westiva-bold}} "Welcome to Pivo"]
       [:> rn/Text {:style {:font-weight   :bold
                            :font-size     24
                            :color text-dark
                            :font-family :westiva-regular
                            :margin-top 10}} "Best beer in town"]
       [Button {:disabled? false
                :on-press (fn [] ((.-navigate navigation) "Login"))
                :style {:background-color text-dark
                        :border-color text-dark
                        :border-width :2
                        :margin-top 20}
                :text-style {:font-family :westiva-bold
                             :color "white"
                             :font-size 20
                             :font-weight :bold}}
        "Login"]
       [Button {:disabled? false
                :on-press (fn [] ((.-navigate navigation) "Register"))
                :style {:background-color pale-yellow
                        :border-color text-dark
                        :border-width :2}
                :text-style {:font-family :westiva-bold
                             :font-size 20
                             :color text-dark
                             :font-weight :bold}}
        "Register"]]]]))
