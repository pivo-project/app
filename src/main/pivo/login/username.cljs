(ns pivo.login.username
  (:require ["react-native" :as rn]
            [pivo.styles.colors :refer [text-dark]]))

(defn UserName [{:keys [username]}]
  [:<>
   [:> rn/Text {:style {:margin-top 50
                        :color text-dark
                        :font-size 24
                        :font-family :westiva-regular}} "Username"]
   [:> rn/TextInput {:style {:font-family :westiva-regular
                             :margin-top 5
                             :border-width :2
                             :border-radius    400
                             :font-size 24
                             :border-color text-dark
                             :padding-horizontal 10
                             :width "100%"
                             :color text-dark}
                     :on-change-text (fn [value] (reset! username value))}]])
