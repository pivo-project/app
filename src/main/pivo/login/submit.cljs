(ns pivo.login.submit
  (:require
   [reagent.core :as r]
   [pivo.components.button :refer [Button]]
   [pivo.graph.client :refer [make-client]]
   [pivo.graph.mutations.login :as mutations]
   [pivo.styles.colors :refer [text-dark]]))

(defn- login [username password]
  (let [client (make-client)]
    ((.-mutate client) #js {:mutation mutations/login
                            :variables #js {:username @username
                                            :password @password}})))

(defn- set-loading [loading] (reset! loading true))

(defn- on-press [username password loading]
  ((set-loading loading)
   (login username password)))

(defn Submit [{:keys [username password navigation]}]
  (let [loading (r/atom false)]
    [Button {:disabled? (or (= @username "") (= @password "") (= @loading true))
             :on-press (fn [] ((.-navigate navigation) "Category"))
             :style {:background-color text-dark
                     :border-color text-dark
                     :border-width :2
                     :margin-top 20}
             :text-style {:font-family :westiva-bold
                          :color "white"
                          :font-weight :bold
                          :font-size 20}}
     "Login"]))
