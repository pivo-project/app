(ns pivo.login.password
  (:require ["react-native" :as rn]
            [pivo.styles.colors :refer [text-dark]]))

(defn Password [{:keys [password]}]
  [:<>
   [:> rn/Text {:style {:margin-top 20
                        :font-size 24
                        :color text-dark
                        :font-family :westiva-regular}} "Password"]
   [:> rn/TextInput {:style {:font-family :westiva-regular
                             :margin-top 5
                             :border-width :2
                             :border-radius    400
                             :font-size 24
                             :border-color text-dark
                             :padding-horizontal 10
                             :width "100%"
                             :color text-dark}
                     :secure-text-entry true
                     :on-change-text (fn [value] (reset! password value))}]])
