(ns pivo.login.title
  (:require ["react-native" :as rn]
            [pivo.styles.colors :refer [text-dark]]))

(defn Title []
  [:> rn/Text {:style {:font-weight   :bold
                       :font-size     32
                       :color text-dark
                       :font-family :westiva-bold}} "Login"])
