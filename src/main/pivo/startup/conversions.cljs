(ns pivo.startup.conversions
  (:require [cljs.core.async :as async :refer [<! >! chan go]]))

;; Might not be needed
(defn promise->channel [promise]
  (let [c (chan)]
    (.then promise (fn [value] (async/go (async/>! c value)) (async/close! c)))
    (.catch promise (fn [error] (async/go (async/>! c {:error error})) (async/close! c)))
    c))
