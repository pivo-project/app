(ns pivo.startup.fontWrapper
  (:require         [reagent.core :as r]
                    ["expo-font" :as expo-fonts]))

(def westiva-bold (js/require "../assets/fonts/westiva-bold.ttf"))
(def westiva-regular (js/require "../assets/fonts/westiva-regular.ttf"))

;; TODO: this gives a "unique key" error
(defn font-wrapper [& children]
  (let [loaded (-> #js {"westiva-bold" westiva-bold "westiva-regular" westiva-regular}
                   (expo-fonts/useFonts)
                   (first))]
    (if loaded (r/as-element children) (r/as-element nil))))
