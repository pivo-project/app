(ns pivo.common.centered-container
  (:require ["react-native" :as rn]
            [pivo.common.dismiss-keyboard :refer [dismiss-keyboard]]
            [pivo.styles.colors :refer [pale-yellow]]))

;; TODO: Fix android behaviour
(defn CenteredContainer [& children]
  [:> rn/KeyboardAvoidingView {:style {:flex 1}
                               :behavior :padding}
   [:> rn/TouchableWithoutFeedback {:on-press dismiss-keyboard}
    [:> rn/View {:style {:flex 1
                         :padding-vertical 50
                         :padding-horizontal 50
                         :align-items :center
                         :justify-content :center
                         :background-color pale-yellow}}
     children]]])
