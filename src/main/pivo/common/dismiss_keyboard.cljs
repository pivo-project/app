(ns pivo.common.dismiss-keyboard
  (:require ["react-native" :as rn]))

(defn dismiss-keyboard [] ((.-dismiss rn/Keyboard)))
