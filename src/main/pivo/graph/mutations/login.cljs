(ns pivo.graph.mutations.login
  (:require ["@apollo/client" :as graph]))

(def login (graph/gql
            "
mutation login($username: String!, $password: String!) {
	login(username: $username, password: $password) {
		accessToken
		refreshToken
	}
}
"))
