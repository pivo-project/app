(ns pivo.graph.mutations.register
  (:require ["@apollo/client" :as graph]))

(def register (graph/gql
               "
mutation register($username: String!, $password: String!) {
	register(username: $username, password: $password) {
		accessToken
		refreshToken
	}
}
"))
