(ns pivo.graph.client
  (:require ["@apollo/client" :as apollo]
            [pivo.startup.constants :refer [pivo-api]]))

(defn- build-client-props []
  #js {:uri pivo-api
       :cache (new (.-InMemoryCache apollo))})

(defn make-client []
  (new (.-ApolloClient apollo) (build-client-props)))
