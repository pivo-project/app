(ns pivo.components.card
  (:require ["react-native" :as rn]
            [reagent.core :as r]))

(defn Card [& children]
  [:> rn/View {:style {:border-radius 4
                       :shadow-radius 4
                       :width 300
                       :shadow-opacity 0.1
                       :background-color "#ffffff"
                       :margin-top 10
                       :margin-bottom 10
                       :padding 10}}
   r/as-element children])
